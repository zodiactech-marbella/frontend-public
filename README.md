
# FrontEnd Developer assignment

***The Games Inventory***

Implement a simple website with a registry of indie games. It should present a list of fictional or real games with details described below, and allow its edit.

Please don't spend more than two afternoons on it ;)

## **Funcional requirements**:

#### 1. Store fictional (or real) game record of:

* title
* description
* preview image URL (can be any online image)
* URL to game (can be fake),
* author email

#### 2. Assure at least two routable views: 

* game list - showing all records with title and preview image minature
* game details - showing all details including preview image

#### 3. Implement options to:

* add (with form validation)
* edit (with form validation)
* remove (after confirmation)

#### 4. Allow direct linking to particular game details page

#### 5. Make sure pages response to window size change with at least two breakpoints (specified later):

* desktop (1024px wide)
* mobile (320px wide)

#### 5. Make sure the app maintains game records even if the browser window is reloaded (it can forget them when browser gets closed)

#### 6. Deploy it to your favourite online hosting service

## **Technical requirements**:

#### 1. Mimic architecture of full scale web application
#### 2. Code preferably with TypeScript
#### 3. Make the UI responsive - layout needs to look good on:
* desktop window size of 1024px wide
* mobile window size of 320px wide
#### 4. Feel free to use any framework or even vanilla code
#### 5. Unit tests are not mandatory but very welcome
#### 6. Provide a link to repository
#### 7. Provide a link to working website